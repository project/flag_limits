<?php

namespace Drupal\flag_limits\Form;

use Drupal\Core\Form\FormStateInterface;
use Drupal\flag\Form\FlagConfirmForm;

/**
 * Provides the confirm form page for flagging an entity.
 *
 * @see \Drupal\flag\Plugin\ActionLink\ConfirmForm
 */
class FlagLimitsConfirmForm extends FlagConfirmForm {

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    try {
      \Drupal::service('flag')->flag($this->flag, $this->entity);
    }
    catch (\LogicException $e) {
      // Fail silently so we return to the entity, which will show an updated
      // link for the existing state of the flag.
      // 304 - Not Modified.
      if ($e->getCode() == 304) {
        $error = $e->getMessage();
        if (!empty($error)) {
          \Drupal::service('messenger')->addWarning($error);
        }
      }
    }
  }

}
