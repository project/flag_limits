<?php

namespace Drupal\flag_limits\EventSubscriber;

use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\flag\FlagCountManager;
use Drupal\flag_limits\Event\FlagLimitsEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class ValidationSubscriber.
 *
 * EventSubscriber to Drupal\flag_limits\Event\FlagLimitsEvent.
 *
 * @package Drupal\flag_limits\EventSubscriber
 */
class ValidationSubscriber implements EventSubscriberInterface {

  use StringTranslationTrait;

  /**
   * FlagCountManager EventSubscriber.
   *
   * @var \Drupal\flag\FlagCountManager
   */
  private $flagCountManager;

  /**
   * ValidationSubscriber constructor.
   *
   * @param \Drupal\flag\FlagCountManager $flag_count_manager
   *   FlagCountManager EventSubscriber.
   */
  public function __construct(FlagCountManager $flag_count_manager) {
    $this->flagCountManager = $flag_count_manager;
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      FlagLimitsEvent::FLAG_LIMITS_FLAG_VALIDATE => 'flagLimitsValidate',
    ];
  }

  /**
   * Validate flag.
   *
   * @param \Drupal\flag_limits\Event\FlagLimitsEvent $event
   *   Flag limits event object.
   */
  public function flagLimitsValidate(FlagLimitsEvent $event) {
    $flag = $event->getFlag();
    // Validate flagging count rer flag by User.
    if ($flag->getThirdPartySetting('flag_limits', 'limited_flag_user')) {
      $max_count = $flag->getThirdPartySetting('flag_limits', 'limit_flag_user_count');
      $default_message = 'You can add only add @max_count flags.';
      $error_message = $flag->getThirdPartySetting('flag_limits', 'limit_flag_user_error', $default_message);
      $count = $this->flagCountManager->getUserFlagFlaggingCount(
        $flag,
        $event->getAccount(),
        $event->getAnonymousSessionId()
      );

      if ($count >= $max_count) {
        $event->setError(
          $this->t($error_message, ['@max_count' => $max_count]),
          FlagLimitsEvent::ERROR_CODE_SHOW_MESSAGE
        );
      }
    }
  }

}
